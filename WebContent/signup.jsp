<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー登録</title>
</head>
<body>
	<jsp:include page="/layout/header.jsp" />
	<div class="pageBody">
		<div class="conteiner">
			<div class="row">
				<div
					class="col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4 col-xl-offset-4 col-xl-4 Body2">

					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages" style="text-align: left; height: auto; width: 100%; background: #ffe6ea; margin-left: auto; margin-right: auto; margin-bottom: 20px; padding-left: 10px; line-height: 50px; border: solid 2px red;">

								<c:forEach items="${errorMessages}" var="message">
									・<c:out value="${message}" /><br>
								</c:forEach>

						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>
					<form action="signup" method="post">
						<br /> <input name="name" id="user-name" value="${user.name}"
							placeholder="名前" /><br /> <input name="account"
							id="user-account" value="${user.account}" placeholder="ログインID" />
						<br /> <input name="password" type="password" id="user-password"
							value="${user.password}" placeholder="パスワード" /> <br /> <input
							name="checkPassword" type="password" id="user-password2"
							value="${checkPassword}" placeholder="確認用パスワード" /> <br />

						<div class="signup-branch">
							<label for="branch_id">所属</label> <select name="branchId"
								id="sinup-select">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${user.branchId==branch.id}">
										<option value="${branch.id}" selected>${branch.name}</option>
									</c:if>
									<c:if test="${user.branchId!=branch.id}">
										<option value="${branch.id}">${branch.name}</option>
									</c:if>
								</c:forEach>
							</select> <br />
						</div>

						<div class="signup-position">
							<label for="position_id">部署・役職</label> <select name="positionId"
								id="sinup-select">
								<c:forEach items="${positions}" var="position">
									<c:if test="${user.positionId==position.id}">
										<option value="${position.id}" selected>${position.name}</option>
									</c:if>"
				<c:if test="${user.positionId!=position.id}">
										<option value="${position.id}">${position.name}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
						<br />


						<button type="submit"
							style="border-radius: 8px; width: 100%; height: 30px; margin-bottom: 20px; background: #534752; color: white;">登録</button>
						<br />
					</form>
					<a href="adminUser">戻る</a>
				</div>

			</div>
		</div>
		<jsp:include page="/layout/footer.jsp" />
	</div>
</body>
</html>