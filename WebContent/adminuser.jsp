<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>ユーザー管理画面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	function stop() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('このアカウントを停止させますか？')) {

			return true;
		}
		// 「OK」時の処理終了

		// 「キャンセル」時の処理開始
		else {

			window.alert('キャンセルされました');

			return false;
		}
		// 「キャンセル」時の処理終了

	}

	function start() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('このアカウントを再開させますか？')) {

			return true;

		}
		// 「OK」時の処理終了

		// 「キャンセル」時の処理開始
		else {

			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
		// 「キャンセル」時の処理終了

	}
</script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<script>
	jQuery(document).ready(function() {
		var offset = 100;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.pagetop').fadeIn(duration);
			} else {
				jQuery('.pagetop').fadeOut(duration);
			}
		});

		jQuery('.pagetop').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({
				scrollTop : 0
			}, duration);
			return false;
		})
	});
</script>


</head>
<body>
	<jsp:include page="/layout/header.jsp" />
	<div class="pageBody">

		<div class="container-fluid">
			<div class="row">
				<div
					class="col-xs-12 col-sm-12 col-md-offset-1 col-md-8 col-lg-offset-1 col-lg-8 col-xl-offset-1 col-xl-8 Body3">

					<div class="users">
						<c:forEach items="${users}" var="user">
							<div class="user">
								<span class="name"><span class="glyphicon glyphicon-user"></span> ID: <c:out value="${user.id}" /></span><br>
								<span class="account">ログインID: <c:out
										value="${user.account}" /></span><br> <span class="name">ユーザー名
									<c:out value="${user.name}" />
								</span><br>
								<%-- <span class="name"><c:out value="${user.password}" /></span> --%>
								<span class="name">配属: <c:out value="${user.branchName}" /></span><br>
								<span class="name">部署・役職: <c:out
										value="${user.positionName}" /></span><br>
										<span>登録日:</span>
								<fmt:formatDate value="${user.createdAt}"
									pattern="yyyy/MM/dd HH:mm:ss" />
								<br>

								<form action="userEdit">
									<input type="hidden" name="userId" value="${user.id}">
									<button type="submit" class="btn userEdit-btn">編集</button>
								</form>

								<c:if test="${loginUser.id != user.id}">
									<c:choose>
										<c:when test="${user.state == 0}">
											<form action="userState" onSubmit="return stop()">
												<input type="hidden" name="userId" value="${user.id}">
												<button type="submit" class="btn stop-btn">アカウント停止</button>
											</form>
										</c:when>
										<c:otherwise>
											<form action="userState" onSubmit="return start()">
												<input type="hidden" name="userId" value="${user.id}">
												<button type="submit" class="btn start-btn">アカウント復活</button>
											</form>
										</c:otherwise>
									</c:choose>
								</c:if>
							</div>
						</c:forEach>
					</div>
					<jsp:include page="/layout/footer.jsp" />
					<div class="pagetop">↑</div>
				</div>
				<jsp:include page="/layout/sidemenu.jsp" />
			</div>
		</div>

	</div>

</body>
</html>
