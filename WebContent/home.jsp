<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="./js/board.js"></script>
<title>掲示板</title>
<script type="text/javascript">
	function dt() {
		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm("削除しますか？")) {
			return true;
		}
		// 「OK」時の処理終了
		// 「キャンセル」時の処理開始
		else {
			window.alert("キャンセルされました");
			return false;
		}
		// 「キャンセル」時の処理終了
	}

</script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<script>
	jQuery(document).ready(function() {
		var offset = 100;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.pagetop').fadeIn(duration);
			} else {
				jQuery('.pagetop').fadeOut(duration);
			}
		});
		jQuery('.pagetop').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({
				scrollTop : 0
			}, duration);
			return false;
		})
	});
</script>

</head>
<body>
	<jsp:include page="/layout/header.jsp" />

	<div class="pageBody">
		<div class="container-fluid">


			<%-- <h4>
			<c:out value="こんにちは、${loginUser.name}さん" />
		</h4> --%>



			<div class="search">
				<form action="./" method="get">
					<input type="date" name="date1" id="dateSearch" value="${date1}" />
					～ <input type="date" name="date2" id="dateSearch" value="${date2}" />
					<input name="categorySearch" id="categorySearch"
						placeholder="カテゴリー部分検索" value="${category}" />
					<button class="btn search-btn" type="submit">Search</button>
				</form>
			</div>

			<div
				class="col-xs-12 col-sm-12 col-md-offset-1 col-md-8 col-lg-offset-1 col-lg-8 col-xl-offset-1 col-xl-8 Body3">

				<c:if test="${ not empty messages }">
					<div class="messages">
						<c:forEach items="${messages}" var="message">
							<div class="homeMessage">
								・
								<c:out value="${message}" />
							</div>
						</c:forEach>
					</div>
					<c:remove var="messages" scope="session" />
				</c:if>

				<div class="posts">
					<c:forEach items="${posts}" var="post" varStatus="s">
						<div class="post">
							<div class="postBody">
								<div class="account-name">
									<div class="profile">
										<span class="glyphicon glyphicon-user"
											style="line-height: 45px; color: white; font-size: 25px;"></span>
									</div>
									<span class="post-name"
										style="line-height: 40px; color: #d20059;"><c:out
											value="${post.name}" /></span>
								</div>
								<div class="title"
									style="display: inline-block; color: #a5887a; margin-right: 20px; margin-left: 13px; line-height: 45px;">
									件名:
									<c:out value="${post.title}" />
								</div>

								<div class="category"
									style="display: inline-block; color: #a5887a; line-height: 45px;">
									カテゴリー:
									<c:out value="${post.category}" />
								</div>


								<div class="grad-wrap">
									<pre class="grad-item">
										<c:out value="${post.content}" />
									</pre>
									<span class="grad-trigger"></span>
								</div>

								<div class="post-date">
									投稿日
									<fmt:formatDate value="${post.created_at}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<c:if test="${post.userId == loginUser.id}">
									<form action="postDelete" onSubmit="return dt()">
										<input type="hidden" name="postId" value="${post.id}">
										<button class="btn btn-link" type="submit" id="trash">
											<span class="glyphicon glyphicon-trash trash"></span>
										</button>
									</form>
								</c:if>

								<br>
							</div>

							<div class="comments">
							<div class='comment-area'>
								<div class='view-area'>
									<c:forEach items="${comments}" var="comment">
										<c:if test="${comment.postId == post.id}">

											<div class="comment-block">

												<div class="comment-account">
													<div class="comment-value">
														<div class="comment-profile" style="vertical-align: top;">
															<span class="glyphicon glyphicon-user"
																style="line-height: 36px; color: white; font-size: 20px;"></span>
														</div>
														<span class="name"><c:out value="${comment.name}" /></span>
														<div class="comment-content"
															style="display: inline-block; vertical-align: top;">
															<pre class="comment-pre">
															<c:out value="${comment.content}" />
														</pre>
														</div>
														<div class="comment-date">
															<fmt:formatDate value="${comment.createdAt}"
																pattern="yyyy/MM/dd HH:mm:ss" />
														</div>

														<c:if test="${comment.userId == loginUser.id}">
															<form action="commentDelete" onSubmit="return dt()">
																<input type="hidden" name="commentId"
																	value="${comment.id}">
																<button class="btn btn-link" type="submit"
																	id="comment-trash">
																	<span class="glyphicon glyphicon-trash comment-trash"></span>
																</button>
															</form>
														</c:if>
													</div>
												</div>
											</div>
										</c:if>
									</c:forEach>
								</div>

								<div class='error-area'>
									<ul></ul>
								</div>

								<%-- <c:if test="${ not empty commentMessages }">
									<div class="messages">
										<c:forEach items="${commentMessages}" var="message">
											<div class="commentMessage">
												・
												<c:out value="${message}" />
											</div>
										</c:forEach>
									</div>
									<c:remove var="commentMessages" scope="session" />
								</c:if>
 --%>
                                <div class='form-area'>
								<form action="newComment" method="post" class="post-comment"
									id='register-form'>
									<textarea name="content" id='text' class="comment-box"
										placeholder="コメントする"></textarea>
									<input type="hidden" name="post_id" value="${post.id}"
										id="post_id"> <input type="hidden"
										value="${loginUser.id}" id="user_id">
									<button class="btn post-comment-btn" type="button"
										id='register'>送信</button>
								</form>
								</div>
								<br>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>

			<jsp:include page="/layout/sidemenu.jsp" />

			<div class="hidden-xs hidden-sm pagetop">↑</div>
			<a class="hidden-md hidden-lg hidden-xl new-button" href="newPost">+<span
				class="glyphicon glyphicon-pencil"></span></a>

		</div>
		<jsp:include page="/layout/footer.jsp" />
	</div>
</body>
</html>