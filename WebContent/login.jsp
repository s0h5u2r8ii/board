<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>ログイン</title>

</head>
<body>

	<div class="hidden-xs hidden-sm"
		style="width: 100%; height: 70px; background: #534752; position: fixed; top: 0px; left: 0px; z-index: 10;">
		<div class="header-title"
			style="line-height: 70px; color: #b29a8e; font-family: 'Comic Sans MS'; font-size: 30px; display: inline-block; margin-left: 45px;">
			Internet Discussion Board <span class="glyphicon glyphicon-cutlery"></span>
		</div>
	</div>

	<div class="hidden-md hidden-lg hidden-xl"
		style="width: 100%; height: 100px; background: #534752; position: fixed; top: 0px; left: 0px; z-index: 10;">
		<div class="header2-title"
			style="color: #b29a8e; font-family: 'Comic Sans MS'; font-size: 35px; margin-left: 45px; line-height: 100px;">
			Internet Discussion Board <span class="glyphicon glyphicon-cutlery"></span>
		</div>

	</div>

	<div class="main-contents"
		style="height: 100vh; width: 100%; background: #f3eade; padding-top: 100px; text-align: center; position: relative;">
		<c:if test="${ not empty loginMessages }">

			<c:forEach items="${loginMessages}" var="message">
				<div class="loginMessages" style="border: solid 2px red; width: 50%; height: 100px; margin-left: auto; margin-right: auto;">
					<div style="margin: auto; line-height: 100px;">・<c:out value="${message}" /></div>
				</div>
			</c:forEach>

			<c:remove var="loginMessages" scope="session" />
		</c:if>

		<c:if test="${ not empty errorMessages }">

			<c:forEach items="${errorMessages}" var="message">
				<div class="messages" style=" line-height: 100px; border: solid 2px red; width: 50%; height: 100px; margin-left: auto; margin-right: auto;">
					・<c:out value="${message}" />
				</div>
			</c:forEach>

			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post">
			<br /> <label for="LoginId"
				style="font-family: 'Comic Sans MS'; font-size: 25px; color: #7e6356;">Login
				ID</label><br> <input name="loginId" id="loginId" value="${account}"
				style="width: 300px; border-radius: 8px;" /> <br /> <label
				for="password"
				style="font-family: 'Comic Sans MS'; font-size: 25px; color: #7e6356;">Password</label><br>
			<input name="password" type="password" id="password"
				style="width: 300px; border-radius: 8px; margin-bottom: 20px;" /> <br />
			<button type="submit"
				style="width: 300px; border-radius: 8px; font-family: 'Comic Sans MS'; font-size: 25px; background: #534752; color: white;">Login</button>
			<br />

		</form>
		<div class="copyright"
			style="position: absolute; bottom: 0; left: 0; right: 0; margin: auto;">Copyright(c)ShuriSato</div>
	</div>
</body>
</html>