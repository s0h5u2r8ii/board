$(document).ready(function() {
	var itemHeights = [];
	var returnHeight;

	$(function() {
		$(".grad-item").each(function() { // ターゲット(縮めるアイテム)
			var thisHeight = $(this).height(); // ターゲットの高さを取得
			itemHeights.push(thisHeight + 50); // それぞれの高さを配列に入れる
			$(this).addClass("is-hide"); // CSSで指定した高さにする
			returnHeight = $(this).height(); // is-hideの高さを取得
		});
	});

	$(".grad-trigger").click(function() { // トリガーをクリックしたら
		if (!$(this).hasClass("is-show")) {
			var index = $(this).index(".grad-trigger"); // トリガーが何個目か
			var addHeight = itemHeights[index]; // 個数に対応する高さを取得
			$(this).addClass("is-show").prev().animate({
				height : addHeight
			}, 200).removeClass("is-hide"); // 高さを元に戻す
		} else {
			$(this).removeClass("is-show").prev().animate({
				height : returnHeight
			}, 200).addClass("is-hide"); // 高さを制限する
		}
	});
	/*
	 * コメント登録の処理
	 */

	/*$('button#register').on('click', function() {*/
	$('#register').click(function(){

		// 送信するデータを用意
		var form = $(this).parents("form#register-form");
		var postId = form.find('#post_id').val();
		var userId = form.find('#user_id').val();
		var content = form.find('#text').val();
		var comment = {'postId' : postId,'userId' : userId,'content' : content};

		// 全てのerror-areaのメッセージを削除する
		$(".error-area").each(function(k, v) {
			$(v).find("ul").empty();
		})

		// Ajax通信処理
		$.ajax({
			dataType : 'json',
			type : "POST",
			url : 'newComment',
			data : {
				comment : JSON.stringify(comment)
			}
		}).done(
				function(data, textStatus, jqXHR) {
					// 成功時の処理
					if (data.is_success == 'true') {
						console.log("ここです1"+data);
						バリデーション通過
						// コメントデータを一覧に追加
						name = $('#user_name').val();
						form.parents("div.comment-area").find(
						"div.view-area").append(
								createCommentBlock(data.userComment));
						// 全てのtextarea要素の中身を削除
						$(".comment-box").each(function(k, v) {
							$(this).val("");
						})
					} else {
						バリデーションエラー
						var errorArea = form.parents("div.comment-area")
						.find("div.error-area");
						// 入力フォームの上にエラーメッセージを表示させる
						data.errors.forEach(function(v, k) {
							// エラーメッセージの要素の数だけ表示させる
							errorArea.find("ul").append(
									"<li>" + v + "</li>");
						});
					}
				}).fail(function(data, textStatus, jqXHR) {
					// 通信失敗時の処理
					console.log("error");
				});
	});

	/*
	 * 表示用データの作成
	 */
	function createCommentBlock(userComment) {
		date = new Date(userComment.createdAt);
		createdAt = date.getFullYear() + '年'
		+ ('0' + (date.getMonth() + 1)).slice(-2) + '月'
		+ ('0' + date.getDate()).slice(-2) + "日 "
		+ ('0' + date.getHours()).slice(-2) + ':'
		+ ('0' + date.getMinutes()).slice(-2) + ':'
		+ ('0' + date.getSeconds()).slice(-2);
		body = "<div class='comment-block'>" + "<div class='content-text'><p>"
		+ userComment.text.replace(/\r?\n/g, '<br />') + "</p></div>"
		+ "<input type='hidden' name='comment_id' value="
		+ userComment.id + " id='comment_id'>"
		+ "<div class='content-info'><p>" + userComment.name + " / "
		+ createdAt + "</p></div>"
		+ "<button type='button' id='delete'>削除</button>" + "</div>";
		return body;
	}

	/*
	 * コメント削除
	 */
	$(document).on("click", "button#delete", function() {
		// 削除するコメントのIDを取得
		var commentBlock = $(this).parents("div.comment-block");
		var id = commentBlock.find('#comment_id').val();

		// 全てのerror-areaのメッセージを削除する
		$(".error-area").each(function(k, v) {
			$(v).find("ul").empty();
		})

		// Ajax通信処理
		$.ajax({
			dataType : 'json',
			type : "POST",
			url : 'comment/delete',
			data : {
				id : id
			}
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if (data.is_success == 'true') {
				/* バリデーション通過 */
				// 画面上のコメントを削除
				commentBlock.remove();
			} else {
				/* バリデーションエラー */
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log('コメント削除できなかった');
		});
	});



});
