<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.account}の編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<jsp:include page="/layout/header.jsp" />
	<div class="pageBody">
		<div class="conteiner">
			<div class="row">
				<div
					class="col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4 col-xl-offset-4 col-xl-4 Body2">
<div
					class="col-xs-offset-1 col-xs-offset-10 col-sm-offset-1 col-sm-offset-10">

					<c:if test="${ not empty errorMessages }">
						<div
									style="text-align: left; height: auto; width: 100%; background: #ffe6ea; margin-left: auto; margin-right: auto; margin-bottom: 20px; padding-left: 10px; line-height: 50px; border: solid 2px red;">

								<c:forEach items="${errorMessages}" var="message">
									・<c:out value="${message}" /><br>
								</c:forEach>

						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>

					<form action="userEdit" method="post" name="userEdit">
						<br /> <input name="id" value="${editUser.id}" id="id"
							type="hidden" />  <input
							name="name" value="${editUser.name}" id="user-name" placeholder="名前" /><br />  <input name="account"
							value="${editUser.account}" placeholder="ログインID" id="user-account"/><br />
						<input name="password" type="password" id="user-password" placeholder="パスワード"/> <br />
						<input name="checkPassword"
							type="password" id="user-password2"placeholder="確認用パスワード" /> <br />


						<c:if test="${editUser.id != loginUser.id}">
						<div class="signup-branch">

							<label for="branchId">所属</label>
							<select name="branchId">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${branch.id == editUser.branchId}">
										<option value="${branch.id}" selected>${branch.name}</option>
									</c:if>
									<c:if test="${branch.id != editUser.branchId}">
										<option value="${branch.id}">${branch.name}</option>
									</c:if>
								</c:forEach>
							</select>
							<br />
							</div>

                          <div class="signup-position">
							<label for="positionId">部署・役職</label>
							<select name="positionId">
								<c:forEach items="${positions}" var="position">
									<c:if test="${position.id == editUser.positionId}">
										<option value="${position.id}" selected>${position.name}</option>
									</c:if>
									<c:if test="${position.id != editUser.positionId}">
										<option value="${position.id}">${position.name}</option>
									</c:if>
								</c:forEach>
							</select>
							<br />
							</div>
						</c:if>
						<input name="state" type="hidden" id="state"
							value="${editUser.state}" /> <button type="submit"
							style="border-radius: 8px; width: 100%; height: 30px; margin-bottom: 20px; background: #534752; color: white;">登録</button>

					</form>
					<a href="adminUser">戻る</a>
				</div>
				</div>
				</div>
				<jsp:include page="/layout/footer.jsp" />
				</div>
</body>
</html>