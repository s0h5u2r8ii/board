<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<div class="hidden-xs hidden-sm copyright">Copyright(c)ShuriSato</div>
	<div class="row">
	<div class="hidden-md hidden-lg hidden-xl footer-menu">
		<c:if test="${ not empty loginUser }">
		<div id="foo" class="col-xs-3 col-sm-3">
			<a href="./" id="footer-menu"><span class="glyphicon glyphicon-home" style="font-size: 45px; display: block;"></span>
				HOME</a>
				</div>
			<c:if test="${loginUser.branchId == 1 && loginUser.positionId == 1}">
			<div id="foo" class="col-xs-3 col-sm-3">
				<a href="adminUser" id="footer-menu"><span
					class="glyphicon glyphicon-user" style="font-size: 45px; display: block;"></span> USERS</a>
					</div>
					<div id="foo" class="col-xs-3 col-sm-3">
				<a href="signup" id="footer-menu"><span
					class="glyphicon glyphicon-edit" style="font-size: 45px; display: block;"></span> SIGN UP</a>
					</div>
			</c:if>
			<div id="foo" class="col-xs-3 col-sm-3">
			<a href="logout" id="footer-menu"><span
				class="glyphicon glyphicon-circle-arrow-right" style="font-size: 45px; display: block;"></span> LOGOUT</a>
				</div>
		</c:if>
	</div>
	</div>


</body>
</html>