<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<div class="hidden-xs hidden-sm col-md-2  col-lg-2 col-xl-2 side">

			<c:if test="${ not empty loginUser }">
			<div id="MENU" style="text-align: center; background: #534752; display: block; color: #b29a8e; font-family: 'Comic Sans MS'; height: 30px; line-height: 30px;">MENU</div>
				<a href="./" id="menu"><span class="glyphicon glyphicon-home"></span>  HOME</a>
				<a href="newPost" id="menu"><span class="glyphicon glyphicon-pencil"></span>  POST</a>
				<c:if test="${loginUser.branchId == 1 && loginUser.positionId == 1}">
					<a href="adminUser" id="menu"><span class="glyphicon glyphicon-user"></span>  USERS</a>
					<a href="signup" id="menu"><span class="glyphicon glyphicon-edit"></span>  SIGN UP</a>
				</c:if>
				<a href="logout" id="menu"><span class="glyphicon glyphicon-circle-arrow-right"></span>  LOGOUT</a>

			</c:if>
					</div>

</body>
</html>