<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>新規投稿</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/layout/header.jsp" />
	<div class="pageBody2">
		<div class="container">
			<div class="row">
				<div
					class="hidden-xs hidden-sm  col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8 col-xl-offset-2 col-xl-6 Body2">

					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">
                            <div
									style="text-align: left; height: auto; width: 90%; background: #ffe6ea; margin-left: auto; margin-right: auto; margin-bottom: 20px; padding-left: 10px; line-height: 50px; border: solid 2px red;">
							<c:forEach items="${errorMessages}" var="message">

									・
									<c:out value="${message}" />

								<br>
							</c:forEach>
							</div>

						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>

					<div class="form-area">
						<form action="newPost" method="post">
							<br /> <input name="title" id="title" placeholder="タイトル"
								value="${post.title}" /> <br /> <input name="category"
								placeholder="カテゴリー" id="category" value="${post.category}" /> <br />
							<textarea name="content" cols="100" rows="5" class="content-box"
								placeholder="本文" id="content">${post.content}</textarea>
							<br />
							<button class="btn post-btn" type="submit">
								投稿する <span class="glyphicon glyphicon-pencil"></span>
							</button>
						</form>
					</div>
				</div>

				<div class="hidden-md hidden-lg hidden-xl col-xs-12 col-sm-12 Body1">

					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages}" var="message">
									<li><c:out value="${message}" />
								</c:forEach>
							</ul>
						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>

					<div class="form-area">
						<c:out value="${post}" />
						<form action="newPost" method="post">
							<br /> <input name="title" id="rs-title" placeholder="件名"
								value="${post.title}" /> <br /> <input name="category"
								placeholder="カテゴリー" id="rs-category" value="${post.category}" />
							<br />
							<textarea name="content" cols="100" rows="5" class="content-box"
								placeholder="本文" id="rs-content">${post.content}</textarea>

							<br />
							<button class="btn rs-post-btn" type="submit">
								投稿する <span class="glyphicon glyphicon-pencil"></span>
							</button>
						</form>
					</div>
				</div>

			</div>
		</div>
		<jsp:include page="/layout/footer.jsp" />
	</div>
</body>
</html>