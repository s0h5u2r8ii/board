package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserPost;
import chapter6.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num, String category, Timestamp date1, Timestamp date2) {

		PreparedStatement ps = null;


		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.category as category, ");
			sql.append("posts.content as content, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_at as created_at ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_at BETWEEN TIMESTAMP( ? ) AND TIMESTAMP( ? )");

			if (category != null) {
				sql.append("AND category like ? ");
			}

			sql.append("ORDER BY created_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setTimestamp(1,  date1);
			ps.setTimestamp(2, date2);

			if (category != null) {
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String content = rs.getString("content");
				Timestamp createdAt = rs.getTimestamp("created_at");

				UserPost post = new UserPost();
				post.setName(name);
				post.setId(id);
				post.setUserId(userId);
				post.setTitle(title);
				post.setCategory(category);
				post.setContent(content);
				post.setCreated_at(createdAt);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
