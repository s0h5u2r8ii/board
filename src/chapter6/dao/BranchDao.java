package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Branch;
import chapter6.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> getBranch(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("id, ");
            sql.append("name ");
            sql.append("FROM branches ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");

                Branch branch = new Branch();
                branch.setName(name);
                branch.setId(id);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


}