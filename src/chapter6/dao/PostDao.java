package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Post;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", category");
			sql.append(", content");
			sql.append(", created_at");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // title
			sql.append(", ?"); // category
			sql.append(", ?"); // content
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, post.getUserId());
			ps.setString(2, post.getTitle());
			ps.setString(3, post.getCategory());
			ps.setString(4, post.getContent());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Post getPosts(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM posts WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<Post> postList = toPostList(rs);

			if (postList.isEmpty() == true) {
				return null;
			} else if (2 <= postList.size()) {
				throw new IllegalStateException("2 <= postList.size()");
			} else {
				return postList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<Post> toPostList(ResultSet rs) throws SQLException {

		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				Timestamp createdAt = rs.getTimestamp("created_at");

				Post post = new Post();
				post.setId(id);
				post.setTitle(title);
				post.setContent(content);
				post.setCategory(category);
				post.setUserId(userId);
				post.setCreatedAt(createdAt);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}

	}

	public void delete(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM posts");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, post.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}