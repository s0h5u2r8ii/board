package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", state");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", 0"); // state
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String account,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				int state = rs.getInt("state");
				Timestamp created_at = rs.getTimestamp("created_at");
				Timestamp updated_at = rs.getTimestamp("updated_at");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setPassword(password);
				user.setBranchId(branch_id);
				user.setPositionId(position_id);
				user.setState(state);
				user.setCreatedAt(created_at);
				user.setUpdatedAt(updated_at);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getUsers(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("users.password as password, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name, ");
            sql.append("users.state as state, ");
            sql.append("users.created_at as created_at ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY created_at ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUsersList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsersList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                String password = rs.getString("password");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                int state = rs.getInt("state");
                Timestamp createdAt = rs.getTimestamp("created_at");

                User user = new User();
                user.setAccount(account);
                user.setName(name);
                user.setId(id);
                user.setPassword(password);
                user.setBranchName(branchName);
                user.setPositionName(positionName);
                user.setState(state);
                user.setCreatedAt(createdAt);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public User getUser(Connection connection, int editUserId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.name as name, ");
            sql.append("users.account as account, ");
            sql.append("users.password as password, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("branches.name as branchName, ");
            sql.append("users.position_id as position_id, ");
            sql.append("positions.name as positionName, ");
            sql.append("users.state as state, ");
            sql.append("users.created_at as created_at ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("WHERE users.id = ? ");
            sql.append("ORDER BY created_at DESC");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, editUserId);

            ResultSet rs = ps.executeQuery();
			List<User> editUserList = toEditUserList(rs);
			if (editUserList.isEmpty() == true) {
				return null;
			} else if (2 <= editUserList.size()) {
				throw new IllegalStateException("2 <= editUserList.size()");
			} else {
				return editUserList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	 private List<User> toEditUserList(ResultSet rs)
	            throws SQLException {

	        List<User> ret = new ArrayList<User>();
	        try {
	            while (rs.next()) {
	                String account = rs.getString("account");
	                String name = rs.getString("name");
	                int id = rs.getInt("id");
	                String password = rs.getString("password");
	                int branchId = rs.getInt("branch_id");
	                String branchName = rs.getString("branchName");
	                int positionId = rs.getInt("position_id");
	                String positionName = rs.getString("positionName");
	                int state = rs.getInt("state");
	                Timestamp createdAt = rs.getTimestamp("created_at");

	                User user = new User();
	                user.setAccount(account);
	                user.setName(name);
	                user.setId(id);
	                user.setPassword(password);
	                user.setBranchId(branchId);
	                user.setBranchName(branchName);
	                user.setPositionId(positionId);
	                user.setPositionName(positionName);
	                user.setState(state);
	                user.setCreatedAt(createdAt);

	                ret.add(user);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }

	public void update(Connection connection, User user, User loginUser) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			if (!user.getPassword().isEmpty()) {
			sql.append(", password = ?");
			}
			if (user.getId() != loginUser.getId()) {
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			}
			sql.append(", state = 0");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			if (!user.getPassword().isEmpty() && user.getId() != loginUser.getId()) {
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());
			ps.setInt(6, user.getId());
			} else if (user.getPassword().isEmpty() && user.getId() != loginUser.getId()){
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
			} else if (!user.getPassword().isEmpty() && user.getId() == loginUser.getId()) {
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getId());
			} else if (user.getPassword().isEmpty() && user.getId() == loginUser.getId()) {
				ps.setInt(3, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void userStop(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" state = 1");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void userStart(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" state = 0");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}