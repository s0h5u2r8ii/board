package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

	public int insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		int autoIncKey = -1;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append(" user_id");
			sql.append(", post_id");
			sql.append(", content");
			sql.append(", created_at");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", ?"); // content
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString(), java.sql.Statement.RETURN_GENERATED_KEYS);

			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getPostId());
			ps.setString(3, comment.getContent());

			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			autoIncKey = getAutoIncrementKey(rs);
			return autoIncKey;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private int getAutoIncrementKey(ResultSet rs)
			throws SQLException {

		int key = -1;
		try {
			if(rs.next()) {
				key = rs.getInt(1);
			}
		} finally {
			close(rs);
		}
		return key;
	}

	public UserComment getComments(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<UserComment> commentList = toCommentList(rs);

			if (commentList.isEmpty() == true) {
				return null;
			} else if (2 <= commentList.size()) {
				throw new IllegalStateException("2 <= postList.size()");
			} else {
				return commentList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toCommentList(ResultSet rs) throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String content = rs.getString("content");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");
				Timestamp createdAt = rs.getTimestamp("created_at");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setContent(content);
				comment.setUserId(userId);
				comment.setPostId(postId);
				comment.setCreatedAt(createdAt);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}


	}

	public UserComment getComment(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("c.id as id, ");
			sql.append("c.user_id as user_id, ");
			sql.append("c.post_id as post_id, ");
			sql.append("u.name as name, ");
			sql.append("c.content as content, ");
			sql.append("c.created_at as created_at, ");
			sql.append("from ");
			sql.append("comments as c ");
			sql.append("inner join ");
			sql.append("users as u ");
			sql.append("ON ");
			sql.append("c.user_id = u.id ");
			sql.append("where c.id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toCommentList(rs);
			return ret.get(0);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public void delete(Connection connection, UserComment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, comment.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}