package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import chapter6.beans.Post;
import chapter6.beans.UserPost;
import chapter6.dao.PostDao;
import chapter6.dao.UserPostDao;

public class PostService {

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost(String category, Timestamp date1, Timestamp date2) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserPostDao postDao = new UserPostDao();

            List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, category, date1, date2);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public Post getPosts(int postsId) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            Post ret = postDao.getPosts(connection, postsId);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public void delete(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.delete(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}