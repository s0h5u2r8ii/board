package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter("/*")


public class LoginFilter implements Filter{
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{

    	String url = ((HttpServletRequest) request).getRequestURI();

    	String path = ((HttpServletRequest) request).getServletPath();


    	HttpSession session = ((HttpServletRequest) request).getSession();
        User user = (User) session.getAttribute("loginUser");


       if(user == null && !(path.contains("/login"))){
        	String messages = "ログインしてください。";
        	session.setAttribute("loginMessages", messages);

        	((HttpServletResponse)response).sendRedirect("login");
        } else {
        	chain.doFilter(request, response);
        }

    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}