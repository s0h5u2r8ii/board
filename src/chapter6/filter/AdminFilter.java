package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns = {"/adminUser", "/signup","/userEdit" })


public class AdminFilter implements Filter{
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{


		HttpSession session = ((HttpServletRequest) request).getSession();

		User user = (User) session.getAttribute("loginUser");

		if (user == null) {
			String adminMessages ="ログインしてください。";
			session.setAttribute("loginMessages", adminMessages);
			((HttpServletResponse)response).sendRedirect("login");
		}else if(!(user.getBranchId() == 1 && user.getPositionId() == 1)) {
			String adminMessages ="権限がありません";
			session.setAttribute("messages", adminMessages);
			((HttpServletResponse)response).sendRedirect("./");
		} else {

			chain.doFilter(request, response);
		}
	}


	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}