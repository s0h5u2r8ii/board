package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/userState" })
public class UserStateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Integer userId = Integer.parseInt(request.getParameter("userId"));

		User user = new UserService().getUser(userId);
		int stateUserId = user.getId();
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		int loginUserId = loginUser.getId();

		if (stateUserId != loginUserId) {

			try {
				new UserService().userState(user);
				request.setAttribute("user", user);
				request.setAttribute("loginUser", loginUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				request.setAttribute("user", user);
				response.sendRedirect("adminUser");
				return;
			}
		}

		response.sendRedirect("adminUser");
	}


}