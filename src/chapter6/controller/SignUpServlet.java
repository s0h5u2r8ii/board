package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Position;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.PositionService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		int branch = Integer.parseInt(request.getParameter("branchId"));

		int position = Integer.parseInt(request.getParameter("positionId"));

		HttpSession session = request.getSession();

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(branch);
		user.setPositionId(position);

		if (isValid(request, messages) == true) {

			new UserService().register(user);

			response.sendRedirect("adminUser");
		} else {
			String checkPassword = request.getParameter("checkPassword");
			Integer branchId = Integer.parseInt(request.getParameter("branchId"));
			Integer positionId = Integer.parseInt(request.getParameter("positionId"));
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("errorMessages", messages);
			request.setAttribute("checkPassword", checkPassword);
			request.setAttribute("branchId", branchId);
			request.setAttribute("positionId", positionId);
			request.setAttribute("positions", positions);
			request.setAttribute("branches", branches);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		String act = null;
		boolean i = false;

		List<User> userList = new UserService().getUsers();

		for (User user :userList) {
			act = user.getAccount();

			System.out.println(act);
			System.out.println(account);

			if (act.equals(account)) {
				i = true;
			}
		}


		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!(account.matches("^[0-9a-zA-Z]+$")) || account.length() > 20 || account.length() < 6) {
			messages.add("ログインIDは半角英数字6～20字で入力してください。");
		} else if (i) {
			messages.add("このログインIDは既に登録されています。");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[-_@+*;:#$%&0-9a-zA-Z]{6,20}$")) {
			messages.add("パスワードは半角英数字6～20字で入力してください。");
		} else if (!checkPassword.equals(password)) {
			messages.add("パスワードと確認用パスワードが一致しません。");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("ユーザー名を入力してください");
		}

		if (name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

		if ((branchId == 1 && positionId == 3) || (branchId == 1 && positionId == 4)) {
			messages.add("本社勤務の場合役職は総務人事担当者または情報管理担当者を選択してください。");
		}

		if ((branchId!=1 && positionId == 1) || (branchId != 1 && positionId == 2)) {
			messages.add("各支店勤務の場合役職は支店長か社員を選択してください。");
		}


		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}