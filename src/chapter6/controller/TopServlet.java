package chapter6.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserPost;
import chapter6.service.CommentService;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();


		User user = (User) request.getSession().getAttribute("loginUser");

		String category = null;

		Timestamp date1 = Timestamp.valueOf("2019-01-01 00:00:00.000000000");

		Timestamp date2 = new Timestamp(System.currentTimeMillis());

		if (StringUtils.isBlank(request.getParameter("categorySearch")) == false) {

			category = request.getParameter("categorySearch");
			request.setAttribute("category", category);
		}

		if (StringUtils.isBlank(request.getParameter("date1")) == false) {
			String param1 = request.getParameter("date1");

			String dt1 = param1 + " 00:00:00.000000000";

				date1 = Timestamp.valueOf(dt1);
				request.setAttribute("date1", param1);
		}

		if (StringUtils.isBlank(request.getParameter("date2")) == false) {
			String param2 = request.getParameter("date2");

				String dt2 = param2 + " 24:00:00.000000000";

				date2 = Timestamp.valueOf(dt2);
				request.setAttribute("date2", param2);
		}

		List<UserPost> posts = new PostService().getPost(category, date1, date2);

		request.setAttribute("posts", posts);

		List<UserComment> comments = new CommentService().getComments();

		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}

}