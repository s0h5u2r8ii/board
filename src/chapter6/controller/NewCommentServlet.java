package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        Comment jsonComment = exportJsonToComment(request);
        System.out.println("投稿ID: "+jsonComment.getPostId() + "ユーザーID"+ jsonComment.getUserId() + "本文"+ jsonComment.getContent());

        List<String> errorMessages = new ArrayList<String>();

        boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

        if (isValid(jsonComment, errorMessages)) {

        	int key = new CommentService().register(jsonComment);
        	UserComment userComment = new CommentService().getComment(key);
        	isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\", \"userComment\" : %s}", isSuccess, exportCommentToJson(userComment));

            User user = (User) session.getAttribute("loginUser");
            response.sendRedirect("./");

        } else {

        	errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
        }

        response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
    }

    private String exportCommentToJson(UserComment userComment) {
		String json = null;

		try {
			json = new ObjectMapper().writeValueAsString(userComment);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return json;
	}

    private Comment exportJsonToComment(HttpServletRequest request) {
		String data = request.getParameter("comment");
		ObjectMapper mapper = new ObjectMapper();

		Comment comment = new Comment();
		try {
			comment = mapper.readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}


		return comment;
	}

    private boolean isValid(Comment comment, List<String> errorMessages) {

        if (StringUtils.isBlank(comment.getContent()) == true) {
        	errorMessages.add("コメントを入力してください");
        }

        if (500 < comment.getContent().length()) {
        	errorMessages.add("本文は500文字以下で入力してください");
        }

        if (errorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowPostForm;
        if (user != null) {
            isShowPostForm = true;
        } else {
            isShowPostForm = false;
        }


        request.setAttribute("isShowPostForm", isShowPostForm);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
