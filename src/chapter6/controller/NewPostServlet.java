package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Post;
import chapter6.beans.User;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");
		Post post = new Post();

		post.setTitle(request.getParameter("title"));
		post.setCategory(request.getParameter("category"));
		post.setContent(request.getParameter("content"));
		post.setUserId(user.getId());

		if (isValid(request, messages) == true) {
			new PostService().register(post);

			response.sendRedirect("./");

		} else {

			request.setAttribute("post", post);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("/post.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String content = request.getParameter("content");
		String title = request.getParameter("title");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}

		if (title.length() > 30) {
			messages.add("件名は30文字以下で入力してください");
		}

		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (category.length() > 10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}

		if (StringUtils.isBlank(content) == true) {
			messages.add("本文を入力してください");
		}
		if (content.length() > 1000) {
			messages.add("本文は1000文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowPostForm;
		if (user != null) {
			isShowPostForm = true;
		} else {
			isShowPostForm = false;
		}


		request.setAttribute("isShowPostForm", isShowPostForm);
		request.getRequestDispatcher("/post.jsp").forward(request, response);
	}
}
