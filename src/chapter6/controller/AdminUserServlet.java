package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.service.UserService;


@WebServlet(urlPatterns = { "/adminUser" })
public class AdminUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 List<User> users = new UserService().getUsers();

        request.setAttribute("users", users);

        request.getRequestDispatcher("/adminuser.jsp").forward(request, response);
    }
}