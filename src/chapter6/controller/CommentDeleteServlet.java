package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.CommentService;;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Integer commentId = Integer.parseInt(request.getParameter("commentId"));
		UserComment comment = new CommentService().getComment(commentId);

		int commentUserId = comment.getUserId();
		User user = (User) request.getSession().getAttribute("loginUser");
		int loginUserId = user.getId();

		if (commentUserId == loginUserId) {

			try {
				new CommentService().delete(comment);
			} catch (NoRowsUpdatedRuntimeException e) {
				response.sendRedirect("./");
				return;
			}
		}


		response.sendRedirect("./");

	}


}
