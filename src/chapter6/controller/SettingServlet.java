package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Position;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.BranchService;
import chapter6.service.PositionService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/userEdit" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		request.setAttribute("loginUser", user);

		if (isParam(request, messages) == true) {

			User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));
			request.setAttribute("editUser", editUser);

			List<Branch> branches = new BranchService().getBranch();
			request.setAttribute("branches", branches);

			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("positions", positions);

			request.getRequestDispatcher("setting.jsp").forward(request, response);
		} else {
			response.sendRedirect("./");
		}
	}

	private boolean isParam(HttpServletRequest request, List<String> messages) {

		HttpSession session = request.getSession();

		if (StringUtils.isBlank(request.getParameter("userId"))==false) {

			String paramId = request.getParameter("userId");

			if (!paramId.matches("^[0-9]*$")) {
				messages.add("不正なパラメータが入力されました。");
				session.setAttribute("messages", messages);
			}else if (new UserService().getUser(Integer.parseInt(request.getParameter("userId"))) == null) {
				messages.add("不正なパラメータが入力されました。");
				session.setAttribute("messages", messages);
			}

		} else if (StringUtils.isBlank(request.getParameter("userId"))==true) {

			messages.add("不正なパラメータが入力されました。");
			session.setAttribute("messages", messages);

		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();

		User editUser = getEditUser(request);

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if (isValid(request, messages) == true) {


			try {
				new UserService().update(editUser, user);
				request.setAttribute("loginUser", user);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}

			request.setAttribute("editUser", editUser);

			response.sendRedirect("adminUser");
		}  else {
			List<Branch> branches = new BranchService().getBranch();
			request.setAttribute("branches", branches);

			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("positions", positions);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}


	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));

		if (user.getId() == Integer.parseInt(request.getParameter("id"))) {
			editUser.setBranchId(user.getBranchId());
			editUser.setPositionId(user.getPositionId());
		} else if (user.getId() != Integer.parseInt(request.getParameter("id"))) {
			editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		}
		editUser.setState(Integer.parseInt(request.getParameter("state")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		int branchId = 0;
		int positionId = 0;

		if (StringUtils.isBlank(request.getParameter("branchId"))==false) {
		branchId = Integer.parseInt(request.getParameter("branchId"));
		}
		if (StringUtils.isBlank(request.getParameter("positionId"))==false) {
		positionId = Integer.parseInt(request.getParameter("positionId"));
		}
		List<String> act = new ArrayList<String>();
		String editUserAccount;
		User edit = null;
		User editUser = null;
		int userId;

		boolean i = false;
		List<User> userList = new UserService().getUsers();

		try {
			edit = getEditUser(request);
		} catch (IOException | ServletException e) {
			// TODO 自動生成された catch ブロック
		}

		userId = edit.getId();
		editUser = new UserService().getUser(userId);
		editUserAccount = editUser.getAccount();

		for (User user :userList) {

			if (!user.getAccount().equals(editUserAccount)) {

				act.add(user.getAccount());
			}
		}

		if (act.contains(account)) {
			i = true;
		}


		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!(account.matches("^[0-9a-zA-Z]+$")) || account.length() > 20 || account.length() < 6) {
			messages.add("ログインIDは半角英数字6～20字で入力してください。");
		} else if (i) {
			messages.add("このログインIDは既に登録されています。");
		}


		if (StringUtils.isBlank(password) == false) {
			if (!password.matches("[-_@+*;:#$%&0-9a-zA-Z]{6,20}$")) {
				messages.add("パスワードは半角英数字6～20字で入力してください。");
			} else if (!checkPassword.equals(password)) {
				messages.add("入力したパスワードと確認用パスワードが一致しません。");
			}
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("ユーザー名を入力してください");
		}

		if (name.length() > 10) {
			messages.add("ユーザー名は10文字以内で入力してください");
		}

        if (branchId != 0 && positionId != 0) {

		if ((branchId == 1 && positionId == 3) || (branchId == 1 && positionId == 4)) {
			messages.add("本社勤務の場合役職は総務人事担当者または情報管理担当者を選択してください。");
		}


		if ((branchId!=1 && positionId == 1) || (branchId != 1 && positionId == 2)) {
			messages.add("各支店勤務の場合役職は支店長か社員を選択してください。");
		}

        }

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}

}