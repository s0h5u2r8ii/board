package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Post;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.PostService;;

@WebServlet(urlPatterns = { "/postDelete" })
public class PostDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Integer postId = Integer.parseInt(request.getParameter("postId"));

		Post post = new PostService().getPosts(postId);
		int postUserId = post.getUserId();
		User user = (User) request.getSession().getAttribute("loginUser");
		int loginUserId = user.getId();

		if (postUserId == loginUserId) {

			try {
				new PostService().delete(post);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.setAttribute("deletePost", post);
				response.sendRedirect("./");
				return;
			}
		}


		response.sendRedirect("./");
	}


}
